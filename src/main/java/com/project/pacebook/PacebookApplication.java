package com.project.pacebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class PacebookApplication {
    public static void main(String[] args) {
        SpringApplication.run(PacebookApplication.class, args);
    }
}
